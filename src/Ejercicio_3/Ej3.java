package Ejercicio_3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ej3 {
    public static void main(String[] args) throws IOException {

        String comando = "java -jar C:\\Users\\adria\\Desktop\\PspDeber\\Ejercicio3\\out\\artifacts\\Ejercicio3_jar\\Ejercicio3.jar";
        List<String> argslist = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argslist);

        Process procees = pb.start();
        OutputStream os = procees.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        Scanner sc = new Scanner(System.in);
        String linea = sc.nextLine();
        Scanner procesoSC = new Scanner(procees.getInputStream());


        while (!linea.equals("finalizar")) {
            bw.write(linea);
            bw.newLine();
            bw.flush();
            System.out.println(procesoSC.nextLine());
            linea = sc.nextLine().toLowerCase();
        }
    }

}
