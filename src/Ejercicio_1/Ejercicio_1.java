package Ejercicio_1;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
public class Ejercicio_1 {
    //Le damos en el Array  el Comando

    private static final String[] commands = {"cmd", "/c", "dir"};
    private static final String MENSAJE = "Tarda Mucho";

    public static void main(String[] args) throws IOException, InterruptedException {
        File fichero = new File("output.txt");
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(fichero));
        Thread.sleep(2000);


        int inicio = (int) System.currentTimeMillis();
        Process process = new ProcessBuilder(commands).start(); // se crea el proceso
        int fin = (int) System.currentTimeMillis();
        int tiempomS = 2000;
        int resta = (tiempomS - (inicio - fin)) / 10;
        System.out.println(resta);


        if (tiempomS > resta) {


            InputStream is = process.getInputStream();
            InputStream ise = process.getErrorStream();

            InputStreamReader isr = new InputStreamReader(is);
            InputStreamReader iser = new InputStreamReader(ise);

            BufferedReader br = new BufferedReader(isr);
            BufferedReader bre = new BufferedReader(iser);

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                dos.writeUTF(line);

            }
            while ((line = bre.readLine()) != null) {
                System.out.println(line);
            }


        } else {
            System.out.println(MENSAJE);
        }

    }
}
